terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
  features {}
  skip_provider_registration = true
  subscription_id = "c7553c90-3ac3-4dcf-94d9-d5b17f8a93ba"
}

resource "azurerm_resource_group" "example" {
  name     = "test_resource_NeSt061123"
  location = "westeurope"
}

resource "azurerm_container_registry" "example" {
  name                = "nextstepcontainerregistry"
  resource_group_name = azurerm_resource_group.example.name
  location            = "West Europe"
  sku                 = "Standard"
  admin_enabled       = true
}


resource "azurerm_storage_account" "example" {
  name                     = "nextstepexamplestoracc"
  resource_group_name      = azurerm_resource_group.example.name
  location                 = azurerm_resource_group.example.location
  account_tier             = "Standard"
  account_replication_type = "ZRS"
}

resource "azurerm_storage_container" "example" {
  name                  = "content"
  storage_account_name  = azurerm_storage_account.example.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "example" {
  name                   = "source_data.zip"
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example.name
  type                   = "Block"
  source                 = "source_data.zip"
}

#resource "azurerm_mysql_server" "example" {
#  name                = "example-mysqlservernextstep"
#  location            = azurerm_resource_group.example.location
#  resource_group_name = azurerm_resource_group.example.name
#
#  administrator_login          = "mysqladminun"
#  administrator_login_password = "H@Sh1CoR3!"
#
#  sku_name   = "B_Gen5_1"
#  storage_mb = 5120
#  version    = "5.7"
#
#  auto_grow_enabled                 = true
#  backup_retention_days             = 7
#  geo_redundant_backup_enabled      = false
#  infrastructure_encryption_enabled = false
#  public_network_access_enabled     = true
#  ssl_enforcement_enabled           = true
#  ssl_minimal_tls_version_enforced  = "TLS1_2"
#}

#resource "azurerm_mysql_database" "example" {
#  name                = "myDatabase"
#  resource_group_name = azurerm_resource_group.example.name
#  server_name         = azurerm_mysql_server.example.name
#  charset             = "utf8"
#  collation           = "utf8_general_ci"
#}
#
resource "azurerm_sql_server" "example" {
  name                         = "myexamplesqlservenextstep0711"
  resource_group_name          = azurerm_resource_group.example.name
  location                     = azurerm_resource_group.example.location
  version                      = "12.0"
  administrator_login          = "4dm1n157r470r"
  administrator_login_password = "4-v3ry-53cr37-p455w0rd"

  tags = {
    environment = "production"
  }
}

resource "azurerm_sql_database" "example" {
  name                = "myexamplesqldatabase"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  server_name         = azurerm_sql_server.example.name

  tags = {
    environment = "production"
  }
}