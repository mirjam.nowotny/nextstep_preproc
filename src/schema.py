from sqlalchemy import Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from nextstep_preproc.connect_db import engine
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class CSVFile(Base):
    __tablename__ = "CSVFile"
    id = Column(
        Integer, primary_key=True, unique=True, autoincrement=True, nullable=False
    )
    name = Column(String(50), unique=True, nullable=True)


class Measurement(Base):
    __tablename__ = "Measurement"
    id = Column(
        Integer, primary_key=True, unique=True, autoincrement=True, nullable=False
    )
    key = Column(String(50), nullable=False)
    channel = Column(Integer, nullable=True)
    pre = relationship("Pre", back_populates="measurement")
    prod = relationship("Prod", back_populates="measurement")


class Topology(Base):
    __tablename__ = "Topology"
    id = Column(
        Integer, primary_key=True, unique=True, autoincrement=True, nullable=False
    )
    from_device_id = Column(Integer, nullable=True)
    to_device_id = Column(Integer, nullable=True)
    pre = relationship("Pre", back_populates="topology")
    prod = relationship("Prod", back_populates="topology")


class Pre(Base):
    __tablename__ = "PreDB"
    id = Column(
        Integer, primary_key=True, unique=True, autoincrement=True, nullable=False
    )
    frequency = Column(Integer, nullable=False)
    topology_key = Column(Integer, ForeignKey("Topology.id"), nullable=False)
    measurement_key = Column(Integer, ForeignKey("Measurement.id"), nullable=False)
    meas = Column(Float, nullable=True)

    topology = relationship("Topology", back_populates="pre")
    measurement = relationship("Measurement", back_populates="pre")


class Prod(Base):
    __tablename__ = "ProdDB"
    id = Column(
        Integer, primary_key=True, unique=True, autoincrement=True, nullable=False
    )
    frequency = Column(Integer, nullable=False)
    topology_key = Column(Integer, ForeignKey("Topology.id"), nullable=False)
    measurement_key = Column(Integer, ForeignKey("Measurement.id"), nullable=False)
    meas = Column(Float, nullable=True)

    topology = relationship("Topology", back_populates="prod")
    measurement = relationship("Measurement", back_populates="prod")


Base.metadata.create_all(engine)
