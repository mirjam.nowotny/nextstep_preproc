from sqlalchemy.orm import sessionmaker
from nextstep_preproc.src.schema import engine, Prod
from utilities.load_prod import load_prod_data

# from nextstep_preproc.ml_service.utilities.find_model import find_best_model

server_name = "localhost"
database_name = "master"


Session = sessionmaker(bind=engine)
session = Session()

df = load_prod_data(session, Prod)

# find_best_model(df)
