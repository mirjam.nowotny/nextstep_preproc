import pandas as pd


def load_prod_data(session, prod):
    result = session.query(prod).execution_options(bulk_load_threshold=1000).all()

    data_list = []
    for entry in result:
        new_entry = {
            "Frequency": entry.frequency,
            "Topology_key": entry.topology_key,
            "Measurement_key": entry.measurement_key,
            "meas": entry.meas,
        }
        data_list.append(new_entry)

    df = pd.DataFrame(data_list)
    return df
