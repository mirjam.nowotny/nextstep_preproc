import h2o
from h2o.automl import H2OAutoML
import matplotlib.pyplot as plt

h2o.init()


def find_best_model(prod_df):
    df = h2o.H2OFrame(prod_df)
    # train, test = df.split_frame(seed=1)
    df.describe()

    y = "meas"
    x = df.columns
    x.remove(y)

    aml = H2OAutoML(max_models=6, seed=1)
    aml.train(x=x, y=y, training_frame=df)
    lb = aml.leaderboard
    top_models = lb.head(rows=3)

    # Optionally, save the top models to a file
    with open("../top_models.txt", "w") as outfile:
        outfile.write("Top Models Information:\n")
        for i, model in enumerate(top_models):
            outfile.write(f"\nTop Model #{i + 1}\n")
            outfile.write(str(model) + "\n")

    # m = aml.leader
    # with open("metrics.txt", "w") as outfile:
    #     outfile.write("LeaderInformation: " + str(m) + "\n")
    #
    # m.learning_curve_plot()
    # plt.title("Learning Curve Plot")
    # plt.savefig("learningCurve.png")
