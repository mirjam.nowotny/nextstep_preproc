from sqlalchemy import create_engine
from dotenv import load_dotenv
import os

load_dotenv()

connection = os.getenv("CONNECTION_STRING")

conn_string = f"{connection}"

engine = create_engine(conn_string)
