from itertools import repeat
import re


def extract_id_from_meas_name(meas_name):
    numbers = [int(number) for number in re.findall(r"(?:ID|ID_)(\d+)", meas_name)]
    while len(numbers) < 2:
        return None
    return numbers[:2]


def extract_key_from_meas_name(meas_name):
    # Determine the key based on suffix
    key = "RAD" if meas_name.endswith("[RAD]") else "V"

    return key


def extract_string_id(name):
    # List of substrings to check for
    substrings_to_check = ["UM", "UN", "US"]

    # Check if any of the substrings is present in the name
    for substring in substrings_to_check:
        if substring in name:
            return substring

    # Return None if none of the substrings is found
    return None


def extract_channel_from_meas_name(meas_name):
    # Extract numbers using regular expression
    numbers = [int(number) for number in re.findall(r"_CH_(\d+)", meas_name)]

    if numbers:
        return numbers[0]  # Return the first number if it exists
    else:
        return None  # Return None if no number is found in the string


def extract_topology(unique_meas_name):
    topology_keys_list = []
    for name in unique_meas_name:
        topology_key = extract_id_from_meas_name(name)
        topology_keys_list.append(topology_key)
    return topology_keys_list


def extract_measurement_key(unique_meas_name):
    measurement_keys_list = []
    for name in unique_meas_name:
        meas_key = extract_key_from_meas_name(name)
        measurement_keys_list.append(meas_key)
    return measurement_keys_list


def extract_measurement_channel(unique_meas_name):
    measurement_channel_list = []
    for name in unique_meas_name:
        measurement_channel = extract_channel_from_meas_name(name)
        measurement_channel_list.append(measurement_channel)
    return measurement_channel_list


def combine_list_name_counts(c_list, name_counts):
    combined_list = [
        value for value, count in zip(c_list, name_counts) for _ in repeat(None, count)
    ]
    return combined_list
