import re
import csv


def extract_from_csv(file_path):
    # Initialize the cell_value variable
    cell_value = None

    # Specify the row and column to read (0-based indexing)
    row_index = 7  # Row 8
    column_index = 0  # Column A
    # Open and read the CSV file
    with open(file_path, newline="") as csvfile:
        reader = csv.reader(csvfile)
        # Iterate through rows
        for i, row in enumerate(reader):
            if i == row_index:
                if column_index < len(row):
                    cell_value = row[column_index]
                    break

    # Check if a value was found and extract the timestamp
    if cell_value is not None:
        # Use regular expressions to extract the timestamp
        timestamp_match = re.search(r"\d{2}\.\d{2}.\d{4}-\d{2}:\d{2}:\d{2}", cell_value)
        if timestamp_match:
            timestamp = timestamp_match.group()

            return timestamp
        else:
            print("Timestamp not found in the specified cell.")
    else:
        print("Value not found in the specified cell.")
