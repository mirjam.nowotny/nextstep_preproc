import re
import csv
from decimal import Decimal


def extract_timestamp(file_path):
    # Initialize the cell_value variable
    cell_value = None

    # Specify the row and column to read (0-based indexing)
    row_index = 7  # Row 8
    column_index = 0  # Column A
    # Open and read the CSV file
    with open(file_path, newline="") as csvfile:
        reader = csv.reader(csvfile)
        # Iterate through rows
        for i, row in enumerate(reader):
            if i == row_index:
                if column_index < len(row):
                    cell_value = row[column_index]
                    break

    # Check if a value was found and extract the timestamp
    if cell_value is not None:
        # Use regular expressions to extract the timestamp
        timestamp_match = re.search(r"\d{2}\.\d{2}.\d{4}-\d{2}:\d{2}:\d{2}", cell_value)
        if timestamp_match:
            timestamp = timestamp_match.group()

            return timestamp
        else:
            print("Timestamp not found in the specified cell.")
    else:
        print("Value not found in the specified cell.")


# # Set the precision to a high value, e.g., 30 digits
# getcontext().prec = 30


def timestamp_from_row(frequency, previous_timestamp):
    # Extract the frequency value from the row

    if frequency == 0:
        return previous_timestamp

    # Calculate the time interval in seconds
    time_interval = 1 / frequency

    # Calculate the new timestamp in floating-point format with nanoseconds
    new_timestamp = Decimal(previous_timestamp) + Decimal(time_interval)

    return Decimal(new_timestamp)


def handle_frequency_conversion(unique_frequencies, unix_base_timestamps, name_counts):
    combined_timestamp_list = []

    for base_timestamp in unix_base_timestamps:
        timestamp_list = []
        for freq in unique_frequencies:
            timestamp = Decimal(timestamp_from_row(freq, base_timestamp))
            timestamp_list.append(Decimal(timestamp))

        combined_timestamp_list.extend(timestamp_list * len(name_counts))

    return combined_timestamp_list
