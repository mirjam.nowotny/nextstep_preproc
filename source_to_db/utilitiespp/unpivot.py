import pandas as pd


def unpivot_csv(file_path):
    df = pd.read_csv(file_path, skiprows=19)

    melted_df = pd.melt(df, id_vars=["f[Hz]"], var_name="meas_name", value_name="meas")

    return melted_df
