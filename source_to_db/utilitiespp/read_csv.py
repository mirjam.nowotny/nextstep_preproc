import os
from nextstep_preproc.source_to_db.utilitiespp.unpivot import unpivot_csv
from nextstep_preproc.source_to_db.utilitiespp.extract_timestamp import extract_from_csv
import pandas as pd
from datetime import datetime


def file_already_read(session, file, files):
    for name in files:
        file_exists = session.query(file).filter(file.name == name).first()
        if file_exists:
            return True
        else:
            return False


def process_csv_file():
    df = pd.DataFrame()
    files = []
    unix_base_timestamps = []
    csv_dir = r"insert/data"

    files_list = os.listdir(csv_dir)
    csv_files = [file for file in files_list if file.endswith(".csv")]

    for file in csv_files:
        try:
            file_path = os.path.join(csv_dir, file)
            # Extract timestamp on file for conversion
            base_timestamp = extract_from_csv(file_path)
            unix_base_timestamp = datetime.strptime(
                base_timestamp, "%d.%m.%Y-%H:%M:%S"
            ).timestamp()
            # basic unpivot dataframe creation
            pivoted_df = unpivot_csv(file_path)

            df = pd.concat([df, pivoted_df], ignore_index=True)
            files.append(file)
            unix_base_timestamps.append(unix_base_timestamp)
        except pd.errors.ParserError:
            print(f"Skipping {file}: Unable to parse the file.")
            continue
    return df, files, unix_base_timestamps
