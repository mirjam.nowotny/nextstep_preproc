from sqlalchemy import Integer, Float


def pre_to_db(engine, df):
    dtype = {
        "Frequency": Integer,
        "Topology_key": Integer,
        "Measurement_key": Integer,
        "meas": Float,
    }
    df.to_sql("PreDB", con=engine, if_exists="append", index=False, dtype=dtype)
