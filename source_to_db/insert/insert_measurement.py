from nextstep_preproc.src.schema import Measurement


def measurement_to_db(session, measurement):
    measurement_id_list = []
    for key, channel in measurement:
        measurement_exists = (
            session.query(Measurement)
            .filter(Measurement.key == key, Measurement.channel == channel)
            .first()
        )
        if measurement_exists is None:
            new_measurement = Measurement(key=key, channel=channel)
            session.add(new_measurement)
            session.commit()
            measurement_id_list.append(new_measurement.id)

        else:
            measurement_id_list.append(measurement_exists.id)
    session.commit()
    return measurement_id_list
