from nextstep_preproc.src.schema import CSVFile


def file_already_read(session, file_names):
    for name in file_names:
        file_exists = session.query(CSVFile).filter(CSVFile.name == name).first()
        if file_exists:
            return True
        else:
            return False


def files_to_db(session, file_names):
    for name in file_names:
        file = CSVFile(name=name)
        session.add(file)
    session.commit()
