from nextstep_preproc.src.schema import Topology


def topology_to_db(session, topology):
    topology_id_list = []
    for from_id, to_id in topology:
        topology_exists = (
            session.query(Topology)
            .filter(
                Topology.from_device_id == from_id,
                Topology.to_device_id == to_id,
            )
            .first()
        )
        if topology_exists is None:
            topology_key = Topology(from_device_id=from_id, to_device_id=to_id)
            session.add(topology_key)
            session.commit()
            topology_id_list.append(topology_key.id)
        else:
            topology_id_list.append(topology_exists.id)

    session.commit()
    return topology_id_list
