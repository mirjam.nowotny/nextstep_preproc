from connect_db import engine
from sqlalchemy.orm import sessionmaker
from utilitiespp.read_csv import process_csv_file

from utilitiespp.extract_features import (
    extract_topology,
    extract_measurement_key,
    extract_measurement_channel,
    combine_list_name_counts,
)
from insert.insert_file import (
    files_to_db,
    file_already_read,
)
from insert.insert_measurement import (
    measurement_to_db,
)
from insert.insert_topology import topology_to_db
from insert.insert_pre import pre_to_db

Session = sessionmaker(bind=engine)
session = Session()

# read csv files to get dataframe and read files
df, files, unix_base_timestamps = process_csv_file()

if not file_already_read(session, files):
    # important variables
    unique_meas_names = df["meas_name"].unique().tolist()
    name_counts = df["meas_name"].value_counts().tolist()
    unique_frequencies = df["f[Hz]"].unique().tolist()
    combined_timestamp_list = df["f[Hz]"]
    # combined_timestamp_list.extend(unique_frequencies * len(name_counts))
    # Convert Datatypes
    # timestamps = handle_frequency_conversion(
    #     unique_frequencies, unix_base_timestamps, name_counts
    # )

    # Feature Engineering
    topology_keys = extract_topology(unique_meas_names, name_counts)
    measurement_keys = extract_measurement_key(unique_meas_names, name_counts)
    measurement_channel = extract_measurement_channel(unique_meas_names, name_counts)

    # DB insert
    files_to_db(session, files)
    measurement = list(zip(measurement_keys, measurement_channel))

    df = df.drop(["meas_name", "f[Hz]"], axis=1)
    df["Frequency"] = combined_timestamp_list

    df["Measurement_key"] = combine_list_name_counts(
        measurement_to_db(session, measurement), name_counts
    )

    df["Topology_key"] = combine_list_name_counts(
        topology_to_db(session, topology_keys), name_counts
    )

    pre_to_db(engine, df)

print("All files have been processed")
