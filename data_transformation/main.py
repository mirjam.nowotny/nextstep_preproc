from utilities.clean_data import clean_data_dpn
from utilities.load_pre import load_pre_data
from insert.insert_prod import prod_to_db
from sqlalchemy.orm import sessionmaker
from nextstep_preproc.src.schema import engine, Pre, Prod


Session = sessionmaker(bind=engine)
session = Session()
row_count_Pre = session.query(Pre).count()
row_count_Prod = session.query(Prod).count()

if not row_count_Pre == row_count_Prod:
    df = load_pre_data(session)
    df = clean_data_dpn(df)

    # df = isolation_forest_sk(df)

    # prod_to_db(engine, df)

print("All files have been processed")
