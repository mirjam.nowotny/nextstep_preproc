def clean_data_dpn(df):
    df.drop_duplicates()

    mean_values = df.mean()
    df.fillna(mean_values)
    return df
