import pandas as pd
from sklearn.ensemble import IsolationForest
import matplotlib.pyplot as plt

# import seaborn as sns


def isolation_forest_sk(df):
    # Train the Isolation Forest model
    iso_forest = IsolationForest(
        contamination=0.02
    )  # Adjust the contamination parameter based on your dataset
    iso_forest.fit(df)

    # Predict outliers
    outlier_preds = iso_forest.predict(df)

    # Add outlier predictions to the DataFrame
    # df["Outlier"] = outlier_preds

    # Visualize the results
    # plt.figure(figsize=(10, 6))
    # sns.scatterplot(
    #     x=df["meas"],
    #     y=df["Frequency"],
    #     hue="Outlier",
    #     data=df,
    #     palette={1: "blue", -1: "red"},
    #     s=100,
    # )
    # plt.title("Isolation Forest Outlier Detection")
    # plt.show()

    return df
